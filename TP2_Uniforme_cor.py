#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt
import math


# réutilisation des fonctions du TP1
def moyechan(tableau):
    return np.sum(tableau)/len(tableau)

def varechan(tableau):
    moy=moyechan(tableau)
    tabmoy=moy*np.ones(len(tableau))
    return (np.sum((tableau-tabmoy)**2))/(len(tableau)-1)

def ecaechan(tableau):
    return np.sqrt(varechan(tableau))

# 1) Création d’un échantillon de 500 nombres (pseudo)aléatoires suivant la loi uniforme sur l’intervalle [0...1[

### 1.1) Création échantillon, histogramme, analyse
# Après avoir créer cet échantillon, que vous nommerez aa, représenter le graphiquement par un histogramme
# normalisé (c.à.d. telle que la surface soit égale à l’unité).
# Calculer la moyenne de aa, un estimateur de sa variance sans biais et un estimateur de son écart type sans biais
# et comparer avec l’espérance, la variance et l’écart type de la loi uniforme (résultats cours-TD).
aa = np.random.uniform(0, 1, 500)
plt.hist(aa, density=1)
plt.show()

def espUniforme(min, max):
    return (min + max) / 2.
def ecaUniforme(min, max):
    return (max - min) / np.sqrt(12)
def varUniforme(min, max):
    return (max - min)**2 / 12

print("Informations sur le tableau de {} nombres (pseudo)aléatoires suivant la loi uniforme :".format(len(aa)))
a, b = moyechan(aa), espUniforme(0, 1)
print(" - moyenne({:.3f})/esperence_uniforme({:.3f}) => {:.3f}, ".format(a, b, a / b))
a, b = varechan(aa), varUniforme(0, 1)
print(" - variance({:.3f})/variance_uniforme({:.3f}) => {:.3f}, ".format(a, b, a / b))
a, b = ecaechan(aa), ecaUniforme(0, 1)
print(" - écart_type({:.3f})/écart_type_uniforme({:.3f}) => {:.3f}, ".format(a, b, a / b))



### 1.2) Étude de la somme de n variables aléatoires indépendantes suivant la loi uniforme sur l’intervalle [0...1[
#### 1.2.1) Programmer une fonction, nommée `so`, telle que `so(n)` donne la somme de `n` variables aléatoires
# indépendantes uniformes sur l’intervalle [0...1[.
def so(n):
    return np.sum(np.random.uniform(0, 1, n))

#### 1.2.2) Programmer une fonction nommée arrayso telle que arrayso(n,m) donne un tableau de m nombres
# dont chacun est le résultat d’un appel à so(n).
# On initiera le tableau par la commande aa=np.empty([1]) suivie de aa[0]=so(n).
# Puis on fera une boucle en utilisant les commandes while et np.append.
def arrayso(n, m):
    aa = np.empty([1])
    aa[0] = so(n)
    i = 1
    while len(aa) < m:
        aa = np.append(aa, so(n))
    return aa


#### 1.2.3) Programmer une fonction nommée `centrallimite` telle que `centrallimite(n,m)` donne un
# tableau de m nombres dont chacun est le résultat d’un appel à `so(n)` que l’on centrera et réduira
# (de façon à ce que l’espérance et la variance soient respectivement 0 et 1).
def centrallimite(n, m):
    aa = arrayso(n, m)
    esp = moyechan(aa)
    eca = np.std(aa)
    aa = (aa - esp) / eca
    return aa

n = 500
m = 1000
aa = centrallimite(n, m)
print("Etude de centrallimite({}, {}) : ".format(n, m))
print(" - espérance : " + str(moyechan(aa)))
print(" - variance : " + str(varechan(aa)))
print(" - min : {}, max : {}".format(np.min(aa), np.max(aa)))




#### 1.2.4) Programmer une fonction nommée `gauss` telle que `gauss(x)` donne la loi de Gauss centrée réduite.
def gauss(x):
    return (1. / np.sqrt(2. * math.pi)) * np.exp(-(np.power(x, 2) / 2))


#### 1.2.5) Reporter sur une même figure :
#  - l’histogramme normé du tableau résultant de l’appel centrallimite(10,10000),
#    (on commencera par des valeurs nettement plus petites pour le second argument pour ne pas avoir un temps de calcul trop long)
#  - le graphe de la loi de Gauss centrée réduite.
#    On prendra pour abscisse l’intervalle [−4...4[ (par exemple avec la commande absci = np.arange(-4, 4.0, 0.1)).
n = 100
m = 10000
aa = centrallimite(n, m)
absci = np.arange(-4, 4.0, 0.1)

fig, ax = plt.subplots()
ax.hist(aa, density=1, label = "centrallimite(V)")
ax.plot(absci, gauss(absci), label = "gauss")
ax.legend(loc='upper right')
ax.set_title('Comparaison de centrallimite(V) avec la loi de gauss'.format(m))
plt.show()
