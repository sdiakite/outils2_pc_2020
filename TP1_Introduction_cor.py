#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt

# 3) Échauffements…
### 3.1) Taper dans une cellule : aa=np.random.rand(6). 
#   Taper aa dans la cellule suivante.
#   Taper ?np.random.rand.
#   Quelle est la signification de aa et son type ?
aa=np.random.rand(6)

help(np.random.rand)

print("aa est un tableau de {} nombres aléatoires (loi uniforme) de type : {}".format(len(aa), type(aa)))

### 3.2.) Programmer une fonction nommée moyechan telle que moyechan(aa) donne la moyenne de l’échantillon aa.
#   (Pour créer une fonction, utiliser def). On utilisera les commandes len et sum.
def moyechan(tableau):
    return sum(tableau)/len(tableau)


### 3.3.) Programmer une fonction nommée varechan telle que varechan(aa) donne la variance (sans biais) de l’échantillon.
#   (On pourra utiliser la commande np.ones).
def varechan(tableau):
    moy=moyechan(tableau)
    tabmoy=moy*np.ones(len(tableau))
    return (sum((tableau-tabmoy)**2))/(len(tableau)-1)

### 3.4.) Programmer une fonction nommée ecaechan telle que ecaechan(aa) donne l’écart type (sans biais) de l’échantillon aa.
#   (Pour la racine carrée, on utilisera la commande np.sqrt)
def ecaechan(tableau):
    return np.sqrt(varechan(tableau))

### 3.5.) Créer un échantillon aa avec la commande aa=np.random.rand(6).
#   Comparer alors le résultat de moyechan, varechan, ecaechan avec les sous-programmes
#   np.mean, np.var, np.std de numpy (vous devez obtenir exactement les mêmes résultats).
aa=np.random.rand(6)
print("aa={}\n\t - moyechan(aa)={}\n\t - np.mean(aa)={}\n\t - moyechan(aa)/np.mean(aa)={}".format(
    aa, moyechan(aa), np.mean(aa), moyechan(aa)/np.mean(aa)))
print("aa={}\n\t - varechan(aa)={}\n\t - n - p.var(aa, ddof=1)={}\n\t - varechan(aa)/np.var(aa, ddof=1)={}".format(
    aa, varechan(aa), np.var(aa, ddof=1), varechan(aa)/np.var(aa, ddof=1)))
print("aa={}\n\t - ecaechan(aa)={}\n\t - n - p.std(aa, ddof=1)={}\n\t - ecaechan(aa)/np.std(aa, ddof=1)={}".format(
    aa, ecaechan(aa), np.std(aa, ddof=1), ecaechan(aa)/np.std(aa, ddof=1)))

### 3.6.) Graphiques : taper d’abord f, ax=plt.subplots() suivi à la ligne dans la même cellule par
#    ax.plot([1,2,3]). Mettre un deuxième graphe sur une seule figure, ainsi qu’un titre (un seul)
#    pour les deux figures ax.set_title(’blabla’)
f, ax = plt.subplots()
ax.plot([1,2,3])
ax.plot([-1,-2,-3,-4])
ax.set_title('Dessin test')
plt.show()

### 3.7) Tracer le graphe de la fonction sinus dans l’intervalle [−4, 4] avec des points d’abscisse espacés de 0.1.
f,ax = plt.subplots()
absci = np.arange(-4, 4.0, 0.1)
ax.plot(absci, np.sin(absci))
ax.set_title('sin(x), avec x dans l\'intervalle [-4 .. 4[')
plt.show()
